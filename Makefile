
.PHONY: inplace test

ifeq ($(OS),Windows_NT)     # is Windows_NT on XP, 2000, 7, Vista, 10...
    PYEXE := py -3
else
    PYEXE := python3
endif


# see test/tag-generator.ipynb
# generates e.g. "cp38-win_amd64"

PYTAG := $(shell $(PYEXE) -c 'import sys; print(f"cp{sys.version_info[0]}{sys.version_info[1]}")')
PLATTAG := $(shell $(PYEXE) -c 'import sysconfig; print(sysconfig.get_platform().replace("-","_"))')
CPSDER := psdest/cpsder.$(PYTAG)-$(PLATTAG).pyd
FFTWDIR := extern/fftw_$(PLATTAG)

all: $(CPSDER)

$(CPSDER): psdest/cpsder.pyx psdest/cpsder.pxd include/psder.h
	python setup.py build_ext --inplace


wheel:
	$(PYEXE) setup.py bdist_wheel

test: $(CPSDER)
	pytest test_basic.py -vvx

bench: $(CPSDER)
	pytest test_benchmark.py --benchmark-sort=name -v --benchmark-autosave

bcross: $(CPSDER)
	pytest test_benchmark.py --benchmark-sort=name -k "cross" -v

bpsd: $(CPSDER)
	pytest test_benchmark.py --benchmark-sort=name -k "psd" -v 

nox:
	nox --stop-on-first-error

clean:
	rm -f psdest/*.pyd psdest/cpsder.cpp rm psdest/*.dll


upload:
	twine upload dist/*.whl