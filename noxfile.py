import nox
nox.options.stop_on_first_error = True

pyvers = [
    '3.10',
    '3.9', '3.9-32',
    '3.8', '3.8-32',
    '3.7', '3.7-32',
    '3.6',
]

@nox.session(python=pyvers)
def build(session):
    session.install('Cython', 'numpy', 'scipy', 'pytest')
    session.run('python', 'setup.py', 'bdist_wheel')
    session.install('--no-index', '--find-links=dist/', 'psdest')
    session.run('pytest', 'test_basic.py')