#!/bin/bash

ORIGPATH=$PATH

for pydir in /c/Python3*; do
    echo $pydir
    export PATH=$pydir/:$pydir/Scripts/:$ORIGPATH
    pip install wheel Cython numpy scipy pytest #&> /dev/null

    make wheel

    pip install --no-index --find-links=dist/ psdest
    make test
    pip uninstall -y psdest &> /dev/null

done
