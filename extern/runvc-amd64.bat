@echo off

echo %~dp0\VCVARS

if exist %~dp0\VCVARS (
    set /p vcvarsloc= < %~dp0\VCVARS
) else (
    set /p vcvarsloc= < %~dp0\VCVARS.default
)

call %vcvarsloc% amd64

%*