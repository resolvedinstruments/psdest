
from libc.stdint cimport uint64_t

cdef extern from "psder.h":
    cdef cppclass PSDer[dtype]:
        PSDer() except +

        void config(uint64_t Nft, uint64_t Nstep, double rate)

        uint64_t Nft()
        uint64_t Nout()
        uint64_t Nstep()
        uint64_t Navg()
        double rate()

        void set_window(dtype *window)

        void cross(dtype *X, dtype *Y, uint64_t N,  dtype *XX, dtype *YY, dtype*XY) nogil
        void psd(dtype *X, uint64_t N, dtype *XX) nogil


    cdef cppclass PSDerFFTW[dtype](PSDer[dtype]):
        PSDerFFTW() except +

    cdef cppclass ThreadedPSDer[dtype](PSDer[dtype]):
        ThreadedPSDer(int nthreads) except +
