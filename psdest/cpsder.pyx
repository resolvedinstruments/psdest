cimport numpy
import numpy
cimport cython
from libc.stdint cimport uint8_t, uint16_t, uint32_t, uint64_t


from scipy import signal



cdef class CPSDer:

    cdef PSDer[double]* _psder
    # # cdef PSDer[float]* _fpsder

    cdef double rate

    cdef object window
    cdef object f



    def __cinit__(self,  Nft=None, rbw=None, rate=1, window='hann', step=0.5, threads=1, dtype=numpy.float64):
        if threads == 1:
            self._psder = new PSDerFFTW[double]()
        elif threads > 1:
            self._psder = new ThreadedPSDer[double](threads)
        else:
            raise RuntimeError(f"Invalid number of threads ({threads})")

        if numpy.dtype(dtype) != numpy.float64:
            raise RuntimeError("This PSDer only supports 64 bit floats")

        self.rate = rate

        if Nft is None:
            Nft = int(rate / rbw)
        else:
            Nft = int(Nft)

        Nstep = int(Nft * step)

        self._psder.config(Nft, Nstep, rate)

        cdef double [::1] cwindow

        if window != 'box':
            window = signal.get_window(window, self.Nft)
            window = window / numpy.mean(window)
            
            cwindow = window
            self._psder.set_window(&cwindow[0])

            

    def __dealloc__(self):
        del self._psder

    @property
    def Nft(self):
        return self._psder.Nft()

    @property
    def Nout(self):
        return self._psder.Nout()

    @property
    def Nstep(self):
        return self._psder.Nstep()

    @property
    def Navg(self):
        return self._psder.Navg()

    @property
    def rate(self):
        return self._psder.rate()

    @property
    def f(self):
        return numpy.fft.rfftfreq(self.Nft, 1./self.rate)

    @property
    def is_complex(self):
        return False


    def psd(self, X):
        cdef uint64_t N = len(X)

        if N < self.Nft:
            raise RuntimeError("array not long enough for 1 fft")

        X = numpy.ascontiguousarray(X)

        XX = numpy.zeros(self.Nout, dtype=numpy.float64)

        cdef double [::1] cX = X
        cdef double [::1] cXX = XX

        with nogil, cython.boundscheck(False):
            self._psder.psd(&cX[0], N, &cXX[0])

        return XX

            
    def cross(self, X, Y):
        if len(X) != len(Y):
            raise RuntimeError("can only do cross correlation on same length arrays")
        if X.dtype != Y.dtype:
            raise RuntimeError("dtypes must be the same")

            
        X = numpy.ascontiguousarray(X)
        Y = numpy.ascontiguousarray(Y)

        cdef uint64_t N = len(X)

        if N < self.Nft:
            raise RuntimeError("array not long enough for 1 fft")
        
        XX = numpy.zeros(self.Nout, dtype=numpy.float64)
        YY = numpy.zeros(self.Nout, dtype=numpy.float64)
        XY = numpy.zeros(self.Nout, dtype=numpy.complex128)

        cdef double [::1] cX = X
        cdef double [::1] cY = Y

        cdef double [::1] cXX = XX
        cdef double [::1] cYY = YY
        cdef double complex [::1] cXY = XY

        with nogil, cython.boundscheck(False):
            self._psder.cross(&cX[0], &cY[0], N, &cXX[0], &cYY[0], <double*>&cXY[0])

        return XX, YY, XY

    




cdef class CPSDer32:

    cdef PSDer[float]* _psder
    # # cdef PSDer[float]* _fpsder

    cdef double rate

    cdef object window
    cdef object f



    def __cinit__(self,  Nft=None, rbw=None, rate=1, window='hann', step=0.5, threads=1, dtype=numpy.float32):
        if threads == 1:
            self._psder = new PSDerFFTW[float]()
        elif threads > 1:
            self._psder = new ThreadedPSDer[float](threads)
        else:
            raise RuntimeError(f"Invalid number of threads ({threads})")

        if numpy.dtype(dtype) != numpy.float32:
            raise RuntimeError("This PSDer only supports 32 bit floats")

        self.rate = rate

        if Nft is None:
            Nft = int(rate / rbw)
        else:
            Nft = int(Nft)

        Nstep = int(Nft * step)

        self._psder.config(Nft, Nstep, rate)

        cdef float [::1] cwindow

        if window != 'box':
            window = signal.get_window(window, self.Nft)
            window = window / numpy.mean(window)
            window = window.astype(numpy.float32)
            
            cwindow = window
            self._psder.set_window(&cwindow[0])

            

    def __dealloc__(self):
        del self._psder

    @property
    def Nft(self):
        return self._psder.Nft()

    @property
    def Nout(self):
        return self._psder.Nout()

    @property
    def Nstep(self):
        return self._psder.Nstep()

    @property
    def Navg(self):
        return self._psder.Navg()

    @property
    def rate(self):
        return self._psder.rate()

    @property
    def f(self):
        return numpy.fft.rfftfreq(self.Nft, 1./self.rate)

    @property
    def is_complex(self):
        return False


    def psd(self, X):
        cdef uint64_t N = len(X)

        if N < self.Nft:
            raise RuntimeError("array not long enough for 1 fft")

        X = numpy.ascontiguousarray(X)

        XX = numpy.zeros(self.Nout, dtype=numpy.float32)

        cdef float [::1] cX = X
        cdef float [::1] cXX = XX

        with nogil, cython.boundscheck(False):
            self._psder.psd(&cX[0], N, &cXX[0])

        return XX

            
    def cross(self, X, Y):
        if len(X) != len(Y):
            raise RuntimeError("can only do cross correlation on same length arrays")
        if X.dtype != Y.dtype:
            raise RuntimeError("dtypes must be the same")


        X = numpy.ascontiguousarray(X)
        Y = numpy.ascontiguousarray(Y)

        cdef uint64_t N = len(X)

        if N < self.Nft:
            raise RuntimeError("array not long enough for 1 fft")

        XX = numpy.zeros(self.Nout, dtype=numpy.float32)
        YY = numpy.zeros(self.Nout, dtype=numpy.float32)
        XY = numpy.zeros(self.Nout, dtype=numpy.complex64)

        cdef float [::1] cX = X
        cdef float [::1] cY = Y

        cdef float [::1] cXX = XX
        cdef float [::1] cYY = YY
        cdef float complex [::1] cXY = XY

        with nogil, cython.boundscheck(False):
            self._psder.cross(&cX[0], &cY[0], N, &cXX[0], &cYY[0], <float*>&cXY[0])

        return XX, YY, XY