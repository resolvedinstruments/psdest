


#ifndef PSDER_H
#define PSDER_H

#include <stdint.h>

#include <thread>
using std::thread;

#include <vector>
using std::vector;

#include <chrono>
using namespace std::chrono_literals;

#include <iostream>
using std::cout;
using std::endl;

#include "fftw3.h"

#include <iostream>
using std::cout;
using std::endl;

#include <vector>
using std::vector;

#include <complex>


// void thread_main() {
//     // i don't know what i'm doing
//     cout << "start" << endl;

//     std::this_thread::sleep_for(2s);

//     cout << "woop" << endl;
// }


template <typename dtype>
class PSDer
{
protected:
    uint64_t _Nft, _Nout, _Nstep;
    uint64_t _Navg; // save number of averages in last calculation
    double _rate;

    dtype *_window;
    bool _window_alloced;


public:
    PSDer()
      : _Nft(0), _Nout(0), _Nstep(0), _rate(1), 
      _window(0), _window_alloced(false)
    {}

    virtual ~PSDer() {
        this->cleanup();
    }


    virtual void cleanup() {
        if (_window > 0 && _window_alloced) {
            fftw_free(_window);
        }
        _window = 0;
        _window_alloced = false;
    }

    virtual void config(uint64_t Nft, uint64_t Nstep, double rate) {
        _Nft = Nft;
        _Nout = Nft / 2 + 1;
        _Nstep = Nstep;
        _rate = rate;

        config_more();

    }

    virtual void config_more() = 0;
    virtual void config_cross() {}

    virtual uint64_t Nft() {
        return _Nft;
    }

    virtual uint64_t Nout() {
        return _Nout;
    }

    virtual uint64_t Nstep() {
        return _Nstep;
    }

    virtual uint64_t Navg() {
        return _Navg;
    }

    virtual double rate() {
        return _rate;
    }


    /*
    Sets window to be used.

    window has to be same length of Nft.
    If window is null, use a box window.

    window is copied to local memory.
    */
    virtual void set_window(dtype *window) {
        if (_window > 0 && _window_alloced) {
            // use fftw to malloc for simd alignment
            fftw_free(_window);
        }
        _window = 0;
        _window_alloced = false;

        if (window > 0) {
            _window = (dtype*) fftw_malloc(sizeof(dtype) * _Nft);
            _window_alloced = true;

            for (uint64_t i = 0, imax = _Nft; i < imax; i++)
                _window[i] = window[i];
        }
            
    }

    /*
     similar to set_window, but don't allocate and copy window.
     that means *window cannot be deallocated until another window is set.
     */
    virtual void set_window_alias(dtype *window) {
        if (_window > 0 && _window_alloced) {
            // use fftw to malloc for simd alignment
            fftw_free(_window);
        }

        _window = window;
        _window_alloced = false;
    }

    virtual void psd(dtype* X, uint64_t N, dtype* XX) = 0;
    virtual void cross(dtype *X, dtype *Y, uint64_t N, dtype *XX, dtype *YY, dtype *XY) = 0;


};


union _fftw_plan {
    fftw_plan plan64;
    fftwf_plan plan32;
};


template <typename dtype>
class PSDerFFTW : public PSDer<dtype>
{
public:
    typedef dtype complex_dtype[2];

protected:
    _fftw_plan _plan1, _plan2;
    dtype  *_in;
    complex_dtype *_out1, *_out2;


    _fftw_plan _fftw_plan_dft_r2c_1d(int n, dtype *in, complex_dtype*out, unsigned flags);
    void _fftw_execute(const _fftw_plan &plan);
    void _fftw_destroy_plan(const _fftw_plan &plan);


public:
    PSDerFFTW() 
      : 
        _plan1(), _plan2(),
        _in(0), 
        _out1(), _out2()
    {

    }

    ~PSDerFFTW()
    {
        cleanup();
    }


    virtual void cleanup()
    {
        PSDer<dtype>::cleanup();

        if (_in > 0) {
            fftw_free(_in);
            _in = 0;
        }
        if (_out1 > 0) {
            fftw_free(_out1);
            _out1 = 0;
        }
        if (_out2 > 0) {
            fftw_free(_out2);
            _out2 = 0;
        }


        _fftw_destroy_plan(_plan1);
        _fftw_destroy_plan(_plan2);

    }



    virtual void config_more() {
        if (_in > 0)
            cleanup();

        _in = (dtype*) fftw_malloc(sizeof(dtype) * _Nft);

        // need 2 outputs/plans for cross PSD calc
        _out1 = (complex_dtype*) fftw_malloc(sizeof(complex_dtype) * _Nout);
        _plan1 = _fftw_plan_dft_r2c_1d((int)_Nft, _in, _out1, FFTW_ESTIMATE);

    }

    virtual void config_cross() {
        if (!_out2) {
            _out2 = (complex_dtype*) fftw_malloc(sizeof(complex_dtype) * _Nout);
            _plan2 = _fftw_plan_dft_r2c_1d((int)_Nft, _in, _out2, FFTW_ESTIMATE);
        }
    }


    virtual void cross(dtype *X, dtype *Y, uint64_t N, dtype *XX, dtype *YY, dtype *XY)
    {
        // lazy configure second output, plan if needed for cross calculation
        if (!_out2)
            config_cross();

        int navg = (int)( (N - _Nft) / _Nstep ) + 1;
        _Navg = navg;
        if (navg <= 0) {
            _Navg = 0;
            return;
        }

        dtype norm = (dtype) ( 2. / (_Nft * navg * _rate) );

        for (int i = 0; i < navg; i++) {
            // first fft
            dtype *Xi = X + i * _Nstep;
            if (_window > 0) {
                // use window
                for (uint64_t j = 0, jmax = _Nft; j < jmax; j++)
                    _in[j] = Xi[j] * _window[j];
            } else {
                // box window
                for (uint64_t j = 0, jmax = _Nft; j < jmax; j++)
                    _in[j] = Xi[j];
            }
            _fftw_execute(_plan1);

            // second fft
            dtype *Yi = Y + i * _Nstep;
            if (_window > 0) {
                // use window
                for (uint64_t j = 0, jmax = _Nft; j < jmax; j++)
                    _in[j] = Yi[j] * _window[j];
            } else {
                // box window
                for (uint64_t j = 0, jmax = _Nft; j < jmax; j++)
                    _in[j] = Yi[j];
            }
            _fftw_execute(_plan2);


            for (uint64_t j = 0, jmax = _Nout; j < jmax; j++) {
                XX[j] += (_out1[j][0] * _out1[j][0] + _out1[j][1] * _out1[j][1]) * norm;
                YY[j] += (_out2[j][0] * _out2[j][0] + _out2[j][1] * _out2[j][1]) * norm;

                XY[2*j + 0] += (_out1[j][0] * _out2[j][0] + _out1[j][1] * _out2[j][1]) * norm;
                XY[2*j + 1] += (_out1[j][1] * _out2[j][0] - _out1[j][0] * _out2[j][1]) * norm;
            }

        }

        // single sided correction
        XX[0] *= 0.5;
        YY[0] *= 0.5;

        XY[0] *= 0.5;
        XY[1] *= 0.5;
    }

    
    virtual void psd(dtype* X, uint64_t N, dtype* XX)
    {
        int navg = (int)( (N - _Nft) / _Nstep ) + 1;
        _Navg = navg;
        if (navg <= 0) {
            _Navg = 0;
            return;
        }

        dtype norm = (dtype)( 2. / (_Nft * navg * _rate) );

        for (int i = 0; i < navg; i++) {

            dtype *Xi = X + i * _Nstep;

            if (_window > 0) {
                // use window
                for (uint64_t j = 0, jmax = _Nft; j < jmax; j++)
                    _in[j] = Xi[j] * _window[j];
            } else {
                // box window
                for (uint64_t j = 0, jmax = _Nft; j < jmax; j++)
                    _in[j] = Xi[j];
            }

            _fftw_execute(_plan1);

            for (uint64_t j = 0, jmax = _Nout; j < jmax; j++)
                XX[j] += (_out1[j][0] * _out1[j][0] + _out1[j][1] * _out1[j][1]) * norm;

        }

        // single sided correction
        XX[0] *= 0.5;
    }



};







template <typename dtype>
class ThreadedPSDer : public PSDer<dtype>
{
public:    

protected:
    int _nthreads;
    vector< PSDer<dtype>* > _psders;

public:
    ThreadedPSDer(int nthreads) : _nthreads(nthreads)
    {
        for (int i = 0; i < _nthreads; i++) {
            PSDer<dtype> *p = new PSDerFFTW<dtype>();
            _psders.push_back(p);
        }
    }

    ~ThreadedPSDer() {

        for (PSDer<dtype>* psder : _psders)
            delete psder;
    }

    virtual void cleanup() {
        PSDer<dtype>::cleanup();

        // pass on to sub psders
        for (auto psder : _psders)
            psder->cleanup();

    }

    virtual void config_more() {
        // config has been called. pass on to sub psders
        for (auto psder : _psders)
            psder->config(_Nft, _Nstep, _rate);
    }

    
    virtual void set_window(dtype *window) {
        // use parent to copy window to local memory
        PSDer<dtype>::set_window(window);

        // tell sub-psders to reference window stored in this object
        for (auto psder : _psders)
            psder->set_window_alias(_window);
    }



    virtual void psd(dtype* X, uint64_t N, dtype* XX)
    {
        // make nthread output buffers
        vector<dtype*> XXs(_nthreads);
        for (int i = 0; i < _nthreads; i++) {
            XXs[i] = new dtype[_Nout];
            
            for (uint64_t j = 0, jmax = _Nout; j < jmax; j++)
                XXs[i][j] = 0;
        }

        // calculate navg per worker thread
        int navg = (int)( (N - _Nft) / _Nstep ) + 1;
        _Navg = navg;
        int navg_per_thread = navg / _nthreads;
        
        vector<int> navgs(_nthreads, navg_per_thread);

        if (_nthreads < _Navg) {
            // too many threads for number of averages.
            for (uint64_t i = _Navg; i < _nthreads; i++)
                navgs[i] = 0;
        }
        // first thread make up for uneven amount of averages
        navgs[0] = navg - navg_per_thread * (_nthreads - 1);

        // call worker threads
        int navg_i = 0; // count how many avgs we've dispatched

        vector<thread> threads;
        for (int i = 0; i < _nthreads; i++) {
            if (navgs[i] == 0)
                continue;

            dtype *Xi = X + navg_i * _Nstep;
            uint64_t Ni = _Nft + _Nstep * (navgs[i] - 1);

            threads.push_back(thread( [=] () {
                _psders[i]->psd(Xi, Ni, XXs[i]);
            } ));


            navg_i += navgs[i];
        }

        for (auto &t : threads)
            t.join();

        // number of averages done by each thread might be different from what we calculated.
        // navg = 0;
        // for (int i = 0; i < _nthreads; i++) {
        //     if (navgs[i] == 0)
        //         continue;

        //     int navg_i = _psders[i]->Navg();

        //     cout << "navg " << i << ": " << navg_i << endl;

        //     if (navgs[i] != navg_i) {
        //         cout << "Warning: thread " << i << " did " << navg_i << " expected " << navgs[i] << endl;
        //     }

        //     navg += navg_i;
        // }
        // _Navg = navg;


        // combine thread results
        for (int i = 0; i < _nthreads; i++) {
            if (navgs[i] == 0)
                continue;
                
            dtype norm = (dtype)( (double)_psders[i]->Navg() / (double)navg );
            
            for (uint64_t j = 0, jmax = _Nout; j < jmax; j++) {
                XX[j] += XXs[i][j] * norm;
            }
        }
    
        

        // cleanup buffers
        for(dtype* b : XXs)
            delete[] b;


    }

    
    virtual void cross(dtype *X, dtype *Y, uint64_t N, dtype *XX, dtype *YY, dtype *XY) {
        // force cross configuration here, as it is not guaranteed to be threadsafe
        for (auto psder : _psders)
            psder->config_cross();

        // make nthread output buffers
        vector<dtype*> XXs(_nthreads);
        vector<dtype*> YYs(_nthreads);
        vector<dtype*> XYs(_nthreads);
        for (int i = 0; i < _nthreads; i++) {
            XXs[i] = new dtype[_Nout];
            YYs[i] = new dtype[_Nout];
            XYs[i] = new dtype[_Nout * 2];
            
            for (uint64_t j = 0, jmax = _Nout; j < jmax; j++)
                XXs[i][j] = 0;
                
            for (uint64_t j = 0, jmax = _Nout; j < jmax; j++)
                YYs[i][j] = 0;
                
            for (uint64_t j = 0, jmax = 2*_Nout; j < jmax; j++)
                XYs[i][j] = 0;
        }



        // calculate navg per worker thread
        int navg = (int)( (N - _Nft) / _Nstep ) + 1;
        _Navg = navg;
        int navg_per_thread = navg / _nthreads;
        
        vector<int> navgs(_nthreads, navg_per_thread);
        if (_nthreads < _Navg) {
            // too many threads for number of averages.
            for (uint64_t i = _Navg; i < _nthreads; i++)
                navgs[i] = 0;
        }
        // first thread make up for uneven amount of averages
        navgs[0] = navg - navg_per_thread * (_nthreads - 1);
        
        // call worker threads
        int navg_i = 0; // count how many avgs we've dispatched

        vector<thread> threads;
        for (int i = 0; i < _nthreads; i++) {
            if (navgs[i] == 0)
                continue;

            dtype *Xi = X + navg_i * _Nstep;
            dtype *Yi = Y + navg_i * _Nstep;
            uint64_t Ni = _Nft + _Nstep * (navgs[i] - 1);

            threads.push_back(thread( [=] () {
                _psders[i]->cross(Xi, Yi, Ni, XXs[i], YYs[i], XYs[i]);
            } ));

            navg_i += navgs[i];
        }

        for (auto &t : threads)
            t.join();

        // combine thread results
        for (int i = 0; i < _nthreads; i++) {
            if (navgs[i] == 0)
                continue;

            dtype norm = (dtype) ((double)_psders[i]->Navg() / (double)navg);
            
            for (uint64_t j = 0, jmax = _Nout; j < jmax; j++) {
                XX[j] += XXs[i][j] * norm;
                YY[j] += YYs[i][j] * norm;
            }

            for (uint64_t j = 0, jmax = 2*_Nout; j < jmax; j++) {
                XY[j] += XYs[i][j] * norm;
            }
        }

        

        // cleanup buffers
        for(dtype* b : XXs)
            delete[] b;
        for(dtype* b : YYs)
            delete[] b;
        for(dtype* b : XYs)
            delete[] b;
    
    }


};


_fftw_plan PSDerFFTW<float>::_fftw_plan_dft_r2c_1d(int n, float *in, PSDerFFTW<float>::complex_dtype*out, unsigned flags)
{
    _fftw_plan plan;
    plan.plan32 = fftwf_plan_dft_r2c_1d(n, in, out, flags);
    return plan;
}

_fftw_plan PSDerFFTW<double>::_fftw_plan_dft_r2c_1d(int n, double *in, PSDerFFTW<double>::complex_dtype*out, unsigned flags)
{
    _fftw_plan plan;
    plan.plan64 = fftw_plan_dft_r2c_1d(n, in, out, flags);
    return plan;
}

void PSDerFFTW<float>::_fftw_execute(const _fftw_plan &plan)
{
    fftwf_execute(plan.plan32);
}

void PSDerFFTW<double>::_fftw_execute(const _fftw_plan &plan)
{
    fftw_execute(plan.plan64);
}

void PSDerFFTW<float>::_fftw_destroy_plan(const _fftw_plan &plan)
{
    fftwf_destroy_plan(plan.plan32);
}

void PSDerFFTW<double>::_fftw_destroy_plan(const _fftw_plan &plan)
{
    fftw_destroy_plan(plan.plan64);
}

#endif // PSDER_H