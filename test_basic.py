

import pytest
import psdest
import numpy as np


@pytest.fixture
def x():
    x = np.random.rand(1024*1024*2)
    return x

psders = [psdest.PSDer, psdest.PyPSDer]
if psdest.CPSDer is not None:
    psders.append(psdest.CPSDer)
# psders that work on complex numbers
cpsders = [psdest.PSDer, psdest.PyPSDer]


@pytest.mark.parametrize('psder', psders)
def test_can_create_psder(psder):
    p = psder(1e3, rate=1e6)


@pytest.mark.parametrize('psder', psders)
def test_psder_sets_from_Nft(psder):
    p = psder(Nft=1e3, rate=1e6)

    assert p.Nft == 1e3


@pytest.mark.parametrize('psder', psders)
def test_psder_sets_Nout(psder):
    p = psder(Nft=1e3, rate=1e6)

    assert p.Nout == int(p.Nft // 2 + 1)

@pytest.mark.parametrize('psder', psders)
def test_pypsd_sets_from_rbw(psder, rbw=1e5, rate=1e6):
    p = psder(rbw=rbw, rate=rate)

    assert p.Nft == int(rate / rbw)
    

@pytest.mark.parametrize('rate', [2, 1e3, 4e5, 4e9])
@pytest.mark.parametrize('psder', psders)
def test_psder_sets_rate(psder, rate):
    p = psder(Nft=100, rate=rate)

    assert p.rate == rate

@pytest.mark.parametrize('psder', psders)
def test_psder_sets_f(psder):
    nft = int(1e3)
    p = psder(Nft=nft, rate=1e6)

    assert all(p.f == np.fft.rfftfreq(nft, 1./1e6))


@pytest.mark.parametrize('psder', psders)
def test_psder_sets_is_complex(psder):
    nft = int(1e3)
    p = psder(Nft=nft, rate=1e6)

    assert p.is_complex == False



@pytest.mark.parametrize('psder', psders)
def test_psder_can_psd(x, psder):
    nft = int(1e3)
    step = 0.5
    nstep  = int(step * nft)
    navg = int((len(x) - nft) / nstep) + 1

    p = psder(Nft=nft, rate=1e6, step=step)
    xx = p.psd(x)

    assert p.Navg == navg
    assert any(xx > 0)


@pytest.mark.parametrize('psder', psders)
def test_psder_can_cross(x, psder):
    nft = int(1e3)
    step = 0.5
    nstep  = int(step * nft)
    navg = int((len(x) - nft) / nstep) + 1

    p = psder(nft, rate=1e6)
    xx, yy, xy = p.cross(x, x)
    
    assert any(xx > 0)
    assert any(yy > 0)
    assert any(xy > 0)
    assert p.Navg == navg



# complex tests

@pytest.mark.parametrize('psder', cpsders)
def test_complex_psder_sets_from_Nft(psder):
    p = psder(Nft=1e3, rate=1e6, dtype=np.complex128)

    assert p.Nft == int(1e3)

@pytest.mark.parametrize('psder', cpsders)
def test_complex_psder_sets_Nout(psder):
    p = psder(Nft=1e3, rate=1e6, dtype=np.complex128)

    assert p.Nout == int(1e3)

@pytest.mark.parametrize('psder', cpsders)
def test_complex_psder_sets_from_rbw(psder, rbw=1e5, rate=1e6):
    p = psder(rbw=rbw, rate=rate, dtype=np.complex128)

    assert p.Nft == int(rate / rbw)
    


@pytest.mark.parametrize('rate', [2, 1e3, 4e5, 4e9])
@pytest.mark.parametrize('psder', cpsders)
def test_complex_psder_sets_rate(psder, rate):
    p = psder(Nft=100, rate=rate, dtype=np.complex128)

    assert p.rate == rate


@pytest.mark.parametrize('psder', cpsders)
def test_complex_psder_sets_f(psder):
    nft = int(1e3)
    p = psder(Nft=nft, rate=1e6, dtype=np.complex128)

    assert all(p.f == np.fft.fftfreq(nft, 1./1e6))


@pytest.mark.parametrize('psder', cpsders)
def test_complex_psder_sets_is_complex(psder):
    nft = int(1e3)
    p = psder(Nft=nft, rate=1e6, dtype=np.complex128)

    assert p.is_complex == True



@pytest.mark.parametrize('psder', cpsders)
def test_complex_psder_can_psd(x, psder):
    nft = int(1e3)
    step = 0.5
    nstep  = int(step * nft)
    navg = int((len(x) - nft) / nstep) + 1

    p = psder(Nft=nft, rate=1e6, step=step, dtype=np.complex128)
    xx = p.psd(x)

    assert p.Navg == navg
    assert len(xx) == nft
    assert any(xx > 0)


@pytest.mark.parametrize('psder', cpsders)
def test_psder_can_cross(x, psder):
    nft = int(1e3)
    step = 0.5
    nstep  = int(step * nft)
    navg = int((len(x) - nft) / nstep) + 1

    p = psder(nft, rate=1e6, dtype=np.complex128)
    xx, yy, xy = p.cross(x, x)

    assert len(xx) == nft
    assert len(yy) == nft
    assert len(xy) == nft
    
    assert any(xx > 0)
    assert any(yy > 0)
    assert any(xy > 0)
    assert p.Navg == navg



# Back to normal PSDer
#

@pytest.mark.parametrize('psder', psders)
def test_psder_works_on_noncontig_array(psder):
    x = np.linspace(0, 1, int(1e6))
    z = x + 5j * x

    p = psder(1e3, rate=1e6)
    p.psd(z.real)
    p.cross(z.real, z.imag)


@pytest.mark.parametrize('psder', psders)
@pytest.mark.parametrize('rate, rbw', [(1e6, 1e3)])
def test_psder_sets_Nft(rate, rbw, psder):
    p = psder(1e3, rate=1e6, step=0.5)

    nft = int(rate // rbw)
    assert p.Nft == 1e6 // 1e3

    assert p.Nstep == (nft * 0.5)

    nout = nft // 2 + 1
    assert p.Nout == nout


# @pytest.mark.parametrize('threads', range(1,11))
# def test_can_c_psd(x, threads):
#     psder = psdest.CPSDer(Nft=20, rate=1e6, threads=threads)
#     xx = psder.psd(x)


# @pytest.mark.parametrize('threads', range(1,11))
# def test_can_c_cross(x, threads):
#     psder = psdest.CPSDer(Nft=1e3, rate=1e6, threads=threads)
#     xx, yy, xy = psder.cross(x, x)


@pytest.mark.parametrize('psder', psders)
def test_can_create_threaded_psder(x, psder):
    p = psder(rbw=1e3, rate=1e6, threads=3)



@pytest.mark.parametrize('window', ['box', 'hann', 'nuttall'])
@pytest.mark.parametrize('threads', [1, 3])
def test_py_and_c_psd_same(x, window, threads):
    if psdest.CPSDer is None:
        pytest.skip("CPSDer not enabled")
    
    py = psdest.PyPSDer(100e3, rate=1e6, window=window)
    c = psdest.CPSDer(100e3, rate=1e6, threads=threads, window=window)

    XX = py.psd(x)
    XX2 = c.psd(x)

    assert all(np.isclose(XX2, XX, rtol=1e-9, atol=1e-9))


@pytest.mark.parametrize('window', ['box', 'hann', 'nuttall'])
@pytest.mark.parametrize('threads', [1, 3])
def test_py_and_c_cross_same(x, window, threads):
    if psdest.CPSDer is None:
        pytest.skip("CPSDer not enabled")
    
    py = psdest.PyPSDer(100e3, rate=1e6, window=window)
    c = psdest.CPSDer(100e3, rate=1e6, window=window, threads=threads)

    XX, YY, XY = py.cross(x, x)
    XX2, YY2, XY2 = c.cross(x, x)

    assert all(np.isclose(XX2, XX, rtol=1e-9, atol=1e-9))
    assert all(np.isclose(YY2, YY, rtol=1e-9, atol=1e-9))
    assert all(np.isclose(XY2, XY, rtol=1e-9, atol=1e-9))
