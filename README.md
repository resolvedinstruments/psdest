# psdest

`psdest` is a module for fast power spectral density (PSD) estimation from time-series data. 

`psdest` provides the following:

- Implementation of [Welch's method](https://en.wikipedia.org/wiki/Welch%27s_method) for power spectral density estimation
- Calibration of the spectral density to Units^2 / Hz
- Easy use of [windowing functions](https://en.wikipedia.org/wiki/Window_function) to reduce [spectral leakage](https://en.wikipedia.org/wiki/Spectral_leakage) when estimating PSD
- A C++ backend for fast, parallelized PSD computation
- A pure Python backend for situations when the C++ backend is not supported

### Installation

`psdest` can be installed [from the PyPI](https://pypi.org/project/psdest/) with
```bash
pip install psdest
```

### Usage

```python
# have some time-series data `z` sampled at 1 MSPS

import psdest

psder = psdest.PSDer(rbw=1e3, rate=1e6)
ZZ = psder.psd(z)
f = psder.f
```

The call to `PSDer` looks like:
```python
PSDer(Nft=None, rbw=None, rate=1., window='hann', step=0.5, threads=1, dtype=np.float64)
```
Where the arguments are:
- `Nft`: Number of input samples per Fourier Transform.  The resulting output spectrum has samples in frequency-space every `rate` / `Nft` Hz.  Only one of `Nft` or `rbw` should be specified.
- `rbw`: Requested frequency spacing of samples in the resulting spectrum.  Equivalent to setting `Nft` to `int(rbw / rate)`.  The actual `rbw` spacing can be found with the `rbw` attribute of the `PSDer`. Only one of `Nft` or `rbw` should be specified.
- `rate`: The samplerate (in Hz) of the data which will be input to the `PSDer`.  Required for proper calibration of the resulting spetrum.
- `window`: The windowing function to be used while computing the PSD. Valid values are any `window` string accepted by [`scipy.signal.get_window()`](https://docs.scipy.org/doc/scipy/reference/generated/scipy.signal.get_window.html)
- `step`: A value in (0, 1] indicating how far the sliding window moves between succesive PSD estimates in [Welch's method](https://en.wikipedia.org/wiki/Welch%27s_method).  A value of 1 is equivalent to using [Bartlett's Method](https://en.wikipedia.org/wiki/Bartlett%27s_method) for PSD estimation.
- `threads`: When supported, the `PSDer` will use this many threads for PSD computation
- `dtype`: The datatype to be used for the input of the PSD estimation. Should be one of `numpy.float32`, `numpy.float64`, `numpy.complex64`, or `numpy.complex128`.  Setting this controls wether real (single-sided) or complex (double-sided) PSDs are computed.  It also sets wether 32 or 64 bit floats are used during PSD estimation.

#### `PSDer` attributes

PSDer exposes the following attributes:

- `Nft`
- `rate`
- `f`

### Limitations

- `CPSDer` currently only supports `numpy.float64` as input datatype.  *i.e.* real, 64-bit transforms.


### Building

`nox` can be used to build modules for multiple Python versions.  Run `nox` to build.

### Benchmarking

`pip install pytest pytest-benchmark`