

from setuptools import setup
from setuptools.extension import Extension

import sys
import os

import sysconfig
PLATTAG = sysconfig.get_platform().replace('-','_')


libraries = []
library_dirs = []
include_dirs = []
package_data = []
ext_modules = []

if PLATTAG.find('win') >= 0:
    # copy correct .dlls to package folder
    import shutil
    shutil.copy(f"extern/fftw_{PLATTAG}/libfftw3-3.dll", "psdest/libfftw3-3.dll")
    shutil.copy(f"extern/fftw_{PLATTAG}/libfftw3f-3.dll", "psdest/libfftw3f-3.dll")

    package_data = ['libfftw3-3.dll', 'libfftw3f-3.dll']


    libraries = ['libfftw3-3', 'libfftw3f-3']
    library_dirs = [f"extern/fftw_{PLATTAG}/"]
    include_dirs = ["include"]

    import numpy
    include_dirs.append(numpy.get_include())

    include_dirs = [os.path.abspath(p) for p in include_dirs]
    library_dirs = [os.path.abspath(p) for p in library_dirs]


    extensions = [
        Extension("psdest.cpsder", ["psdest/cpsder.pyx"],
            define_macros=[("NPY_NO_DEPRECATED_API", "NPY_1_7_API_VERSION")],
            include_dirs = include_dirs,
            libraries = libraries,
            library_dirs = library_dirs,
            extra_compile_args = ['/O2', '/arch:AVX'], #, '/Qvec-report:1'],
            language="c++"),
    ]

    from Cython.Build import cythonize
    ext_modules = cythonize(extensions, language_level="3")

setup(
    name = "psdest",
    version='0.1.0',

    packages = ['psdest'],
    package_data = {'psdest': package_data},
    ext_modules = ext_modules,

    # PyPI metadata
    description='PSD estimation',
    author='Callum Doolin',
    author_email='callum@resolvedinstruments.com',
    url = "https://gitlab.com/resolvedinstruments/psdest",
    license = "MIT",
    python_requires= '>=3.6',
    install_requires = ['numpy', 'scipy'],
    # project_urls={
    # },
    classifiers=[
    'License :: OSI Approved :: MIT License',
    'Programming Language :: Python :: 3.6',
    'Programming Language :: Python :: 3.7',
    'Programming Language :: Python :: 3.8',
    'Programming Language :: Python :: 3.9',
    'Programming Language :: Python :: 3.10',
    'Environment :: Win32 (MS Windows)',
    'Operating System :: POSIX :: Linux',
    'Operating System :: Microsoft :: Windows',
    'Topic :: Scientific/Engineering',
    ],

)
