

import pytest
import psdest
import numpy as np

Nmax = 1024 * 1024 * 100
Nfts = [int(i) for i in np.logspace(0, 1, 4).round(0).astype(int)]
# Nfts = [1024*1024 * 10]



data = np.random.rand(Nmax)




@pytest.mark.parametrize('nft', Nfts)
def test_py_psd(benchmark, nft):
    nft = 1024 * 1024 * nft
    psder = psdest.PyPSDer(Nft=nft, rate=8e7)

    @benchmark
    def bench():
        psder.psd(data[:nft*10])


@pytest.mark.parametrize('nft', Nfts)
@pytest.mark.parametrize('threads', [1, 2, 4, 6, 8])
def test_c_psd(benchmark, nft, threads):
    nft = 1024 * 1024 * nft
    psder = psdest.CPSDer(Nft=nft, rate=8e7, threads=threads)

    @benchmark
    def bench():
        psder.psd(data[:nft*10])




# Cross

@pytest.mark.parametrize('nft', Nfts)
def test_py_cross(benchmark, nft):
    nft = 1024 * 1024 * nft
    psder = psdest.PyPSDer(Nft=nft, rate=8e7)

    @benchmark
    def bench():
        psder.cross(data[:nft*10], data[-nft*10:])

@pytest.mark.parametrize('nft', Nfts)
@pytest.mark.parametrize('threads', [1, 2, 4, 6, 8])
def test_c_cross(benchmark, nft, threads):
    nft = 1024 * 1024 * nft
    psder = psdest.CPSDer(Nft=nft, rate=8e7, threads=threads)

    @benchmark
    def bench():
        psder.cross(data[:nft*10], data[-nft*10:])


if __name__ == '__main__':
    print(Nfts)